import util.LoggerUtil;

public class Runner {

  public static void main(String[] args) {

    Person p = new Person();
    LoggerUtil.log(p, "foo");
    p.foo(2);
    LoggerUtil.log(p, "bar");
    p.bar(3);

  }
}
