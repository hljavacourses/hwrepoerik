package util;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
public class RestLogger {

  @AroundInvoke
  public Object collectBasicLoggingInformation(InvocationContext context) throws Exception{

    Logger logger = LoggerFactory.getLogger(context.getClass());
    logger.info("Method Called: " + context.getMethod().getName());
    logger.info("Parameters: " + context.getParameters().toString());
    return context.proceed();

  }
}
