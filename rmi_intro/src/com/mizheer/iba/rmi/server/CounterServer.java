package com.mizheer.iba.rmi.server;

import com.mizheer.iba.rmi.impl.CounterImpl;
import com.mizheer.iba.rmi.proxy.Counter;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class CounterServer {

  public static void main(String[] args) throws RemoteException, AlreadyBoundException {
    Counter counter = new CounterImpl();
    Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);

    registry.bind(Counter.NAME, counter);
    System.out.println("running");
  }
}
