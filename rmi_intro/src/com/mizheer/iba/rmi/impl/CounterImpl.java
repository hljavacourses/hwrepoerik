package com.mizheer.iba.rmi.impl;

import com.mizheer.iba.rmi.proxy.Counter;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CounterImpl extends UnicastRemoteObject implements Counter {

  private static final long serialVersionUID = 1L;
  private int counter;

  public CounterImpl() throws RemoteException {
  }

  @Override
  public synchronized int reset() throws RemoteException {
    return this.counter = 0;
  }

  @Override
  public synchronized int increment() throws RemoteException {
    return ++this.counter;
  }
}
