package com.mizheer.iba.rmi.proxy;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Counter extends Remote {

  String NAME = Counter.class.getSimpleName();

  int reset() throws RemoteException;

  int increment() throws RemoteException;
}
